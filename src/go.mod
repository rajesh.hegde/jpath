module gitlab.com/encyclopaedia/jpath

go 1.16

require (
	github.com/TylerBrock/colorjson v0.0.0-20200706003622-8a50f05110d2
	github.com/buger/jsonparser v1.1.1
	github.com/fatih/color v1.13.0 // indirect
	github.com/hokaccha/go-prettyjson v0.0.0-20211117102719-0474bc63780f // indirect
	github.com/sirupsen/logrus v1.8.1
)
